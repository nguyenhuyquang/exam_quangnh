<?php
function merge_1_3($str,$n,$char){
    while($n>0){
        $str +=$char;
        $n--;
    }
}
function merge_5_8($str,$n,$char){
    while($n>0){
        $str +=$char;
        $n--;
    }
}
function integerToRoman($n)
{   
    $string ='';
    $hangNghin = intval($n/1000);
    $n = $n%1000;
    $hangTram = intval ($n/100);
    $n = $n%100;
    $hangChuc =intval($n/10);
    $hangDonVi = $n % 10;
    
    while($hangNghin>0){
        $string = $string.'M';
        $hangNghin--;
    }
    if($hangTram == 4){
        $string = $string.'CD';
    } else if($hangTram == 9){
        $string = $string.'CM';
    } else if($hangTram>0 && $hangTram<4){
        while($hangTram>0){
            $string = $string.'C';
            $hangTram--;
        }
    }
    else if($hangTram>=5 && $hangTram<9){
        $string = $string. 'D';
        $hangTramTemp = $hangTram-5;
        while($hangTramTemp>0){
            $string = $string.'C';
            $hangTramTemp--;
        }
    }
    if($hangChuc == 4){
        $string = $string.'XL';
    } else if ($hangChuc == 9){
        $string = $string.'XC';
    } else if($hangChuc>0 && $hangChuc<4){
        while($hangChuc>0){
            $string = $string.'X';
            $hangChuc--;
        }
    }
    else if($hangChuc>=5 && $hangChuc<9){
        $string = $string. 'L';
        $hangChucTemp = $hangChuc-5;
        while($hangChucTemp>0){
            $string = $string.'X';
            $hangChucTemp--;
        }
    }
    if($hangDonVi == 4){
        $string = $string.'IV';
    } else if($hangDonVi == 9){
        $string = $string.'IX';
    } else if($hangDonVi>0 && $hangDonVi<4){
        while($hangDonVi>0){
            $string = $string.'I';
            $hangDonVi--;
        }
    }
    else if($hangDonVi>=5 && $hangDonVi<9){
        $string = $string. 'V';
        $hangDonViTemp = $hangDonVi-5;
        while($hangDonViTemp>0){
            $string = $string.'I';
            $hangDonViTemp--;
        }
    }
    return $string;
}
