<?php

function normalize($str)
{
    $temp ='';
    $len = strlen($str);
    if($str[0]!=' '){
        $temp .= $str[0];
    }
    for($i = 1;$i <$len-1;$i++){

        if($str[$i]!=' '){
            $temp .= $str[$i];
        } else if($str[$i]==' ' && $str[$i+1]!=' '){
            $temp .= ' ';
        }
    }
    if($str[$len-1]!=' '){
        $temp .= $str[$len-1];
    }
    return trim($temp);
}
