<?php

function calc($a,$b){
    if($b == false){
        return $a;
    } else {
        return -1;
    }
}

function calcDeadline($manday, $calendar) {
    $keys = array_keys($calendar);
    $values = array_values($calendar);
    $result = array_map('calc',$keys,$values);
    $unique = array_unique($result);
    $offset = array_search(-1,$unique);
    array_splice($unique,$offset,1);
    $len = count($unique);
    if($manday<=$len){
        return $unique[$manday-1]+1;
    } else {
        return false;
    }
}
