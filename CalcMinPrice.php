<?php
function calcMinPrice($price, $stepPrices, $rivalryPrice) {
    $keyPrices = array_keys($stepPrices);
    $valuePrices = array_values($stepPrices);
    $len = count($keyPrices);
    for($i = 0; $i < $len;$i++){
        if($price<$keyPrices[$i] && $keyPrices[$i] < $rivalryPrice ){
            $temp =ceil(($keyPrices[$i] - $price)/$valuePrices[$i-1]);
            $price += ($temp * $valuePrices[$i-1]);
        } else if ($price<$keyPrices[$i] && $keyPrices[$i] > $rivalryPrice ){
            if($price < $rivalryPrice ){
                $temp =ceil(($rivalryPrice - $price)/$valuePrices[$i-1]);
                $price += ($temp * $valuePrices[$i-1]);
            } else{
                $price += $valuePrices[$i-1];
            }
            
        } 
    }
    return $price;
}

