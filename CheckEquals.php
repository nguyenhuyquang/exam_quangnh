<?php
function array_sort($array = array()){
    for($i = 0; $i< (count($array)-1);$i++){
        for($j = $i+1;$j < count($array);$j++){
            if($array[$i] > $array[$j]){
                $temp = $array[$i];
                $array[$i]=$array[$j];
                $array[$j]=$temp;
            }
        }     
    }
    return $array;
} 
function equals($a,$b){
    if($a == $b)
        return 1;
    else 
        return 0;
}
function checkEquals($a, $b)
{
    if(count($a)==count($b)){
        if(count($a)==0)
            return true;
        else {
            $a_temp = array_sort($a);
            $b_temp = array_sort($b);
            $result = array_map('equals',$a_temp,$b_temp);
            foreach($result as $i){
                if($i==0)
                    return false;
                else return true;
            }
        }   
    } else{
        return false;
    }
}